package ru.alexeyzhulin.smsreceiver.presentation

import android.app.ActivityManager
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ru.alexeyzhulin.smsreceiver.domain.usecases.CheckServiceIsRunningUseCase
import ru.alexeyzhulin.smsreceiver.domain.usecases.SendCardDataUseCase
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val checkServiceIsRunningUseCase: CheckServiceIsRunningUseCase,
    private val sendCardDataUseCase: SendCardDataUseCase

) : ViewModel(){
    var isServiceConnected = MutableLiveData<Boolean>()

    fun checkServiceState(serviceClass: Class<*>, activityManager: ActivityManager): Boolean{
        return checkServiceIsRunningUseCase(serviceClass, activityManager)
    }
}