package ru.alexeyzhulin.smsreceiver.presentation

import android.os.Bundle
import android.view.LayoutInflater
import androidx.lifecycle.lifecycleScope
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import ru.alexeyzhulin.smsreceiver.R
import ru.alexeyzhulin.smsreceiver.common.BaseActivity
import ru.alexeyzhulin.smsreceiver.databinding.ActivitySettingsBinding
import ru.alexeyzhulin.smsreceiver.domain.data.SettingsData
import javax.inject.Inject

@AndroidEntryPoint
class SettingsActivity : BaseActivity() {

    @Inject
    lateinit var settingsViewModel: SettingsViewModel

    private var _binding: ActivitySettingsBinding? = null
    private val binding: ActivitySettingsBinding
        get() = _binding ?: throw RuntimeException("ActivitySettingsBinding is null")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivitySettingsBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)
        lifecycleScope.launch {
            initValues()
        }
    }

    private suspend fun initValues() {
        val settingsData = settingsViewModel.getSettingsData()
        binding.serverLinkText.setText(settingsData.url)
        binding.myNumberText.setText(settingsData.myNumber)
        binding.sberText.setText(settingsData.sber)
        binding.tinkoffText.setText(settingsData.tinkoff)
        binding.otherNumberText.setText(settingsData.extNumber)

        val currentSettingsData = settingsViewModel.getSettingsData()
        binding.saveButton.setOnClickListener {
            lifecycleScope.launch {
                settingsViewModel.setSettingData(
                    SettingsData(
                        url = if (binding.serverLinkText.text.isEmpty()) currentSettingsData.url else binding.serverLinkText.text.toString(),
                        myNumber = binding.myNumberText.text.toString(),
                        sber = binding.sberText.text.toString(),
                        tinkoff = binding.tinkoffText.text.toString(),
                        extNumber = binding.otherNumberText.text.toString()
                    ),
                )
                finish()
            }
        }
    }
}