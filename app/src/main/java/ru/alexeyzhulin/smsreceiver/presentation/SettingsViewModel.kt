package ru.alexeyzhulin.smsreceiver.presentation

import androidx.lifecycle.ViewModel
import ru.alexeyzhulin.smsreceiver.domain.data.SettingsData
import ru.alexeyzhulin.smsreceiver.domain.usecases.GetSettingsDataUseCase
import ru.alexeyzhulin.smsreceiver.domain.usecases.SetSettingsDataUseCase
import javax.inject.Inject

class SettingsViewModel @Inject constructor(
    private val setSettingsDataUseCase: SetSettingsDataUseCase,
    private val getSettingsDataUseCase: GetSettingsDataUseCase
) : ViewModel() {

    suspend fun getSettingsData(): SettingsData {
        return getSettingsDataUseCase()
    }

    suspend fun setSettingData(settingsData: SettingsData) {
        setSettingsDataUseCase(settingsData)
    }
}