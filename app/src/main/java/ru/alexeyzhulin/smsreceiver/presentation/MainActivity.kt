package ru.alexeyzhulin.smsreceiver.presentation

import android.Manifest
import android.app.ActivityManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import ru.alexeyzhulin.smsreceiver.R
import ru.alexeyzhulin.smsreceiver.common.ApplicationService
import ru.alexeyzhulin.smsreceiver.common.BaseActivity
import ru.alexeyzhulin.smsreceiver.databinding.ActivityMainBinding
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : BaseActivity() {
    private var granted = false
    private var _binding: ActivityMainBinding? = null
    private val binding: ActivityMainBinding
        get() = _binding ?: throw RuntimeException("ActivitySettingsBinding is null")
    private lateinit var manager: ActivityManager


    @Inject
    lateinit var mainViewModel: MainViewModel

    lateinit var applicationService: ApplicationService

    private val serviceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName) {
            Log.d("MainActivity", "Disconnected from service")
            mainViewModel.isServiceConnected.value = false
        }

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            Log.d("MainActivity", "Connected to service")
            val binder = service as ApplicationService.ApplicationServiceBinder
            applicationService = binder.getService()
            mainViewModel.isServiceConnected.value = true
        }
    }

    private val requestSmsPermission =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
            granted = permissions.entries.all {
                it.value
            }
            if (granted) {
                sendLog("User granted sms permission")
            } else {
                sendLog("User denied sms permission")
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sendLog("MainActivity created")
        _binding = ActivityMainBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)
        manager =
            getSystemService(androidx.appcompat.app.AppCompatActivity.ACTIVITY_SERVICE) as ActivityManager
    }

    override fun onStart() {
        super.onStart()
        sendLog("MainActivity started")
        //restartApplicationService()
        checkSmsPermiison()
        lifecycleScope.launch {
            initValues()
        }
    }

    private fun initValues() {
        setButtonState(null)
        if (mainViewModel.checkServiceState(SERVICE_CLASS, manager)) {
            val intent = Intent(this, SERVICE_CLASS)
            bindService(
                intent,
                serviceConnection,
                Context.BIND_AUTO_CREATE
            )
        } else {
            mainViewModel.isServiceConnected.value = false
        }

        binding.buttonStart.setOnClickListener {
            toggleService()
        }

        mainViewModel.isServiceConnected.observe(this) {
            setButtonState(mainViewModel.isServiceConnected.value)
        }
    }

    private fun toggleService() {
        if (mainViewModel.checkServiceState(SERVICE_CLASS, manager)) {
            unbindService(serviceConnection)
            val intent = Intent(this, SERVICE_CLASS)
            intent.action = ApplicationService.ACTION_STOP_SERVICE
            startService(intent)
            mainViewModel.isServiceConnected.value = false
        } else {
            val intent = Intent(this, SERVICE_CLASS)
            ContextCompat.startForegroundService(this, intent)
            bindService(
                intent,
                serviceConnection,
                Context.BIND_AUTO_CREATE
            )
            mainViewModel.isServiceConnected.value = null
        }
    }

    private fun setButtonState(serviceIsStarted: Boolean?) {
        with(binding.buttonStart) {
            if (serviceIsStarted == null) {
                isEnabled = false
                text = getString(R.string.waiting)
                return
            }
            if (serviceIsStarted) {
                isEnabled = true
                text = getString(R.string.stop)
            } else {
                isEnabled = true
                text = getString(R.string.start)
            }
        }
    }

    override fun onStop() {
        super.onStop()
        sendLog("MainActivity stopped")
    }

    override fun onDestroy() {
        super.onDestroy()
        sendLog("MainActivity destroyed")
    }

    private fun checkSmsPermiison() {
        when (checkSelfPermission(Manifest.permission.RECEIVE_SMS)) {
            PackageManager.PERMISSION_GRANTED -> {
                sendLog("Sms permission was granted earlier")
            }
            else -> {
                requestSmsPermission.launch(
                    arrayOf(
                        Manifest.permission.RECEIVE_SMS,
                    )
                )
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflator = MenuInflater(this)
        inflator.inflate(R.menu.main_menu, menu)
        return true;
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val i = Intent(this, SettingsActivity::class.java)
        when (item.itemId) {
            R.id.settings -> {
                startActivity(i)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        private val SERVICE_CLASS = ApplicationService::class.java
    }
}