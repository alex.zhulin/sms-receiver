package ru.alexeyzhulin.smsreceiver.common

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.provider.Telephony
import android.telephony.SmsMessage
import android.util.Log

class ApplicationBroadcastReceiver(private val service: ApplicationService): BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action.equals("android.provider.Telephony.SMS_RECEIVED")){
            Log.d("ApplicationBroadcastReceiver", "SMS_RECEIVED")
            var sms = service.processSmsMessage(getSmsMessageFromIntent(intent))
            println(sms)
        }
    }

    private fun getSmsMessageFromIntent(intent: Intent?): SmsMessage? {
        if (intent == null) return null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val messages = Telephony.Sms.Intents.getMessagesFromIntent(intent)
            if (messages.isEmpty()) {
                return null
            }
            return messages[0]
        } else {
            return null
        }
    }
}