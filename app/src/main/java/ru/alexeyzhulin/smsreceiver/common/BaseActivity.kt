package ru.alexeyzhulin.smsreceiver.common

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
abstract class BaseActivity: AppCompatActivity() {

    @Inject
    lateinit var baseViewModel: BaseViewModel

    // Функция логирования в activity
    protected fun sendLog(logValue: String) {
        lifecycleScope.launch {
            baseViewModel.sendLogData(logValue)
        }
    }
}