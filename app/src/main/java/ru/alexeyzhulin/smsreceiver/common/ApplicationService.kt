package ru.alexeyzhulin.smsreceiver.common

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Intent
import android.content.IntentFilter
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.provider.Telephony
import android.telephony.SmsMessage
import androidx.core.app.NotificationCompat
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import ru.alexeyzhulin.smsreceiver.domain.usecases.SendCardDataUseCase
import ru.alexeyzhulin.smsreceiver.domain.usecases.SendLogDataUseCase
import javax.inject.Inject

@AndroidEntryPoint
class ApplicationService : Service() {

    private val job = SupervisorJob()
    private val scope = CoroutineScope(Dispatchers.IO + job)

    @Inject
    lateinit var sendLogDataUseCase: SendLogDataUseCase
    @Inject
    lateinit var sendCardDataUseCase: SendCardDataUseCase

    private val binder = ApplicationServiceBinder()
    private lateinit var applicationBroadcastReceiver: ApplicationBroadcastReceiver

    override fun onBind(intent: Intent?): IBinder {
        return binder
    }

    override fun onCreate() {
        super.onCreate()
        sendLogData("ApplicationService created")

        registerBroadcastReceiver()
        createNotificationChannel()
        startForeground(NOTIFICATION_ID, createNotification())
        registerReceiver(applicationBroadcastReceiver, IntentFilter(Telephony.Sms.Intents.SMS_RECEIVED_ACTION))
    }

    override fun onDestroy() {
        super.onDestroy()
        sendLogData("ApplicationService destroyed")
        unregisterReceiver(applicationBroadcastReceiver)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent?.action == ACTION_STOP_SERVICE) {
            stopForeground(true)
            stopSelf()
        }
        return super.onStartCommand(intent, flags, startId)
    }

    fun processSmsMessage(smsMessage: SmsMessage?) {
        sendLogData("Received sms message")
        if (smsMessage == null) {
            sendLogData("smsMessage is null")
            return
        }
        sendCardData(smsMessage)
        sendLogData("address = ${smsMessage.originatingAddress}, body = ${smsMessage.messageBody}")
        //sendLogDataUseCase(smsMessage)
    }

    private fun sendCardData(smsMessage: SmsMessage){
        scope.launch {
            sendCardDataUseCase(smsMessage.originatingAddress.toString(), smsMessage.messageBody)
        }
    }

    private fun sendLogData(logValue: String) {
        scope.launch {
            sendLogDataUseCase(logValue)
        }
    }

    private fun registerBroadcastReceiver() {
        applicationBroadcastReceiver = ApplicationBroadcastReceiver(this)
    }

    private fun createNotificationChannel() {
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(
                CHANNEL_ID,
                CHANNEL_NAME,
                NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }

    private fun createNotification() = NotificationCompat.Builder(this, CHANNEL_ID)
        .setContentTitle("Sms receiver")
        .setContentText("Sms listener background service")
        .setSmallIcon(com.google.android.material.R.drawable.notification_icon_background)
        .build()

    companion object {

        private const val CHANNEL_ID = "sms_receiver_id"
        private const val CHANNEL_NAME = "Sms receiver"
        private const val NOTIFICATION_ID = 1
        const val ACTION_STOP_SERVICE = "ACTION_STOP_SERVICE"
    }

    inner class ApplicationServiceBinder : Binder() {
        fun getService(): ApplicationService = this@ApplicationService
    }

}