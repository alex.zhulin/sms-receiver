package ru.alexeyzhulin.smsreceiver.common

import androidx.lifecycle.ViewModel
import ru.alexeyzhulin.smsreceiver.domain.usecases.SendLogDataUseCase
import javax.inject.Inject

class BaseViewModel @Inject constructor(private val sendLogDataUseCase: SendLogDataUseCase): ViewModel() {
    suspend fun sendLogData(logValue: String) {
        sendLogDataUseCase(logValue)
    }
}