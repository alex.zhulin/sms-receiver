package ru.alexeyzhulin.smsreceiver.common

import androidx.datastore.core.CorruptionException
import androidx.datastore.core.Serializer
import com.google.gson.Gson
import ru.alexeyzhulin.smsreceiver.domain.data.SettingsData
import java.io.InputStream
import java.io.OutputStream

object SettingsDataSerializer : Serializer<SettingsData> {
    override val defaultValue = SettingsData()

    override suspend fun readFrom(input: InputStream): SettingsData {
        try {
            return Gson().fromJson(input.readBytes().decodeToString(), SettingsData::class.java)
        } catch (serialization: Exception) {
            throw CorruptionException("Unable to read User", serialization)
        }
    }

    override suspend fun writeTo(t: SettingsData, output: OutputStream) {
        output.write(Gson().toJson(t).encodeToByteArray())
    }
}