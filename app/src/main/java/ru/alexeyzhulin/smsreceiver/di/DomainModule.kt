package ru.alexeyzhulin.smsreceiver.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ru.alexeyzhulin.smsreceiver.common.ApplicationBroadcastReceiver
import ru.alexeyzhulin.smsreceiver.data.CardRepositoryImpl
import ru.alexeyzhulin.smsreceiver.data.LogRepositoryImpl
import ru.alexeyzhulin.smsreceiver.data.SettingsDataRepositoryImpl
import ru.alexeyzhulin.smsreceiver.domain.repo.CardRepository
import ru.alexeyzhulin.smsreceiver.domain.repo.LogRepository
import ru.alexeyzhulin.smsreceiver.domain.repo.SettingsDataRepository
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface DomainModule {
    @Binds
    @Singleton
    fun bindLogRepository(logRepositoryImpl: LogRepositoryImpl): LogRepository

    @Binds
    @Singleton
    fun bindCardRepository(cardRepositoryImpl: CardRepositoryImpl): CardRepository

    @Binds
    @Singleton
    fun bindCSettingsDataRepository(cardRepositoryImpl: SettingsDataRepositoryImpl): SettingsDataRepository

}