package ru.alexeyzhulin.smsreceiver.di

import android.content.Context
import android.util.Log
import androidx.datastore.core.DataStore
import androidx.datastore.core.DataStoreFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.alexeyzhulin.smsreceiver.SmsReceiverApplication
import ru.alexeyzhulin.smsreceiver.SmsReceiverApplication.Companion.SHARED_PREFERENCES_FILE_NAME
import ru.alexeyzhulin.smsreceiver.common.ApplicationService
import ru.alexeyzhulin.smsreceiver.common.SettingsDataSerializer
import ru.alexeyzhulin.smsreceiver.data.datasources.ApiDatasource
import ru.alexeyzhulin.smsreceiver.data.datasources.ApiDatasourceImpl
import ru.alexeyzhulin.smsreceiver.data.datasources.LocalStorageDatasource
import ru.alexeyzhulin.smsreceiver.data.datasources.LocalStorageDatasourceImpl
import ru.alexeyzhulin.smsreceiver.data.retrofit.ApiCardService
import ru.alexeyzhulin.smsreceiver.data.retrofit.ApiLogService
import ru.alexeyzhulin.smsreceiver.domain.data.SettingsData
import java.io.File
import java.util.concurrent.TimeUnit

@Module
@InstallIn(SingletonComponent::class)
class DataModule {
    @Provides
    fun bindApiDatasource(apiDatasourceImpl: ApiDatasourceImpl): ApiDatasource {
        return apiDatasourceImpl
    }
    @Provides
    fun provideService(): ApplicationService{
        return ApplicationService()
    }

    @Provides
    fun bindLocalStorageDatasource(localStorageDatasourceImpl: LocalStorageDatasourceImpl): LocalStorageDatasource {
        return localStorageDatasourceImpl
    }

    @Provides
    fun provideRetrofit(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .client(client)
            .baseUrl(SmsReceiverApplication.BASE_LOG_URL)
            .addConverterFactory(GsonConverterFactory.create(SmsReceiverApplication.gson))
            .build()
    }

    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        // HttpLoggingInterceptor
        val httpLoggingInterceptor = HttpLoggingInterceptor { message ->
            Log.d("HttpLoggingInterceptor", message)
        }
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        return OkHttpClient.Builder()
            //httpLogging interceptor for logging network requests
            .addInterceptor(httpLoggingInterceptor)
            .writeTimeout(5, TimeUnit.SECONDS)
            .readTimeout(5, TimeUnit.SECONDS)
            .connectTimeout(10, TimeUnit.SECONDS)
            .build()
    }

    @Provides
    fun provideSettingsDatastore(
        @ApplicationContext context: Context,
    ): DataStore<SettingsData> {
        return DataStoreFactory.create(
            serializer = SettingsDataSerializer,
            produceFile = { File(context.filesDir, SHARED_PREFERENCES_FILE_NAME) })
    }

    @Provides
    fun provideApiLogService(retrofit: Retrofit): ApiLogService {
        return retrofit.create(ApiLogService::class.java)
    }

    @Provides
    fun provideApiCardService(retrofit: Retrofit): ApiCardService {
        return retrofit.create(ApiCardService::class.java)
    }
}