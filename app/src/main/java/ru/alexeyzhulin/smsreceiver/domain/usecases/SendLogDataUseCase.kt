package ru.alexeyzhulin.smsreceiver.domain.usecases

import ru.alexeyzhulin.smsreceiver.SmsReceiverApplication.Companion.APPLICATION_NAME
import ru.alexeyzhulin.smsreceiver.domain.data.LogData
import ru.alexeyzhulin.smsreceiver.domain.data.OperationResult
import ru.alexeyzhulin.smsreceiver.domain.repo.LogRepository
import javax.inject.Inject

class SendLogDataUseCase @Inject constructor(private val logRepository: LogRepository) {
    suspend operator fun invoke(logValue: String): OperationResult<Void> {
        return logRepository.sendLogData(
            LogData(
                application = APPLICATION_NAME,
                logValue = logValue
            )
        )
    }
}