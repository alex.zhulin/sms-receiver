package ru.alexeyzhulin.smsreceiver.domain.data

import ru.alexeyzhulin.smsreceiver.domain.data.enums.Result

/**
 * Результат взаимодействия с внешним сервисом
 */
data class OperationResult<T>(
    val operationResult: Result,
    val resultObject: T? = null,
    val operationInfo: String? = null
)