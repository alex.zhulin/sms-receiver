package ru.alexeyzhulin.smsreceiver.domain.data.enums

enum class Result {
    OK, ERROR, WARNING
}