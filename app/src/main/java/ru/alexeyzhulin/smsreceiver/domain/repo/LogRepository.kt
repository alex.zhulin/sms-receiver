package ru.alexeyzhulin.smsreceiver.domain.repo

import ru.alexeyzhulin.smsreceiver.domain.data.LogData
import ru.alexeyzhulin.smsreceiver.domain.data.OperationResult

interface LogRepository {
    suspend fun sendLogData(logData: LogData): OperationResult<Void>
}