package ru.alexeyzhulin.smsreceiver.domain.repo

import ru.alexeyzhulin.smsreceiver.domain.data.CardData
import ru.alexeyzhulin.smsreceiver.domain.data.OperationResult

interface CardRepository {
    suspend fun sendCardData(url: String, cardData: CardData): OperationResult<Void>
}