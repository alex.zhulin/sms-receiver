package ru.alexeyzhulin.smsreceiver.domain.usecases

import android.app.ActivityManager
import javax.inject.Inject

class CheckServiceIsRunningUseCase @Inject constructor() {
    operator fun invoke(serviceClass: Class<*>, activityManager: ActivityManager): Boolean {

        activityManager.getRunningServices(Integer.MAX_VALUE).forEach { service ->
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

}