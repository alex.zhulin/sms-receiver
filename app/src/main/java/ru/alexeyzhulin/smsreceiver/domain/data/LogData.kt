package ru.alexeyzhulin.smsreceiver.domain.data

data class LogData(
    val application: String,
    val logValue: String,
)