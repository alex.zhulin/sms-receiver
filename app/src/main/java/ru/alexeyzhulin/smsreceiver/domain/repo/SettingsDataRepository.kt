package ru.alexeyzhulin.smsreceiver.domain.repo

import ru.alexeyzhulin.smsreceiver.domain.data.SettingsData

interface SettingsDataRepository {
    suspend fun getSettingsData(): SettingsData

    suspend fun setSettingsData(settingsData: SettingsData)
}