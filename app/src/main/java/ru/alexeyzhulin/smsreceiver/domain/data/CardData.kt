package ru.alexeyzhulin.smsreceiver.domain.data

import com.google.gson.annotations.SerializedName

data class CardData(
    @SerializedName(value = "my_number")
    val myNumber: String,
    @SerializedName(value = "sender_number")
    val senderNumber: String,
    val card: String,
    val text: String,
)