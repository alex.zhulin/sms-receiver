package ru.alexeyzhulin.smsreceiver.domain.data

import ru.alexeyzhulin.smsreceiver.SmsReceiverApplication.Companion.BASE_URL

data class SettingsData(
    val url: String = BASE_URL,
    val myNumber: String = "",
    val sber: String = "",
    val tinkoff: String = "",
    val extNumber: String = "",
)