package ru.alexeyzhulin.smsreceiver.domain.usecases

import ru.alexeyzhulin.smsreceiver.domain.data.CardData
import ru.alexeyzhulin.smsreceiver.domain.data.OperationResult
import ru.alexeyzhulin.smsreceiver.domain.repo.CardRepository
import ru.alexeyzhulin.smsreceiver.domain.repo.SettingsDataRepository
import javax.inject.Inject

class SendCardDataUseCase @Inject constructor(
    private val cardRepository: CardRepository,
    private val settingsDataRepository: SettingsDataRepository
) {
    suspend operator fun invoke(senderNumber: String, text: String): OperationResult<Void> {
        val settingsData = settingsDataRepository.getSettingsData()
        return cardRepository.sendCardData(
            url = settingsData.url,
            cardData = CardData(
                myNumber = settingsData.myNumber,
                senderNumber = senderNumber,
                card = when (senderNumber) {
                    "900" -> {
                        settingsData.sber
                    }
                    "Tinkoff" -> {
                        settingsData.tinkoff
                    }
                    else -> {
                        "0000-0000-0000-0000"
                    }
                },
                text = text
            )
        )
    }
}