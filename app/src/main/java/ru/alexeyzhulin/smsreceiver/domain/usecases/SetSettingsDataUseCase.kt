package ru.alexeyzhulin.smsreceiver.domain.usecases

import ru.alexeyzhulin.smsreceiver.domain.data.SettingsData
import ru.alexeyzhulin.smsreceiver.domain.repo.SettingsDataRepository
import javax.inject.Inject

class SetSettingsDataUseCase @Inject constructor(private val settingsDataRepository: SettingsDataRepository) {
    suspend operator fun invoke(settingsData: SettingsData) {
        settingsDataRepository.setSettingsData(settingsData)
    }
}