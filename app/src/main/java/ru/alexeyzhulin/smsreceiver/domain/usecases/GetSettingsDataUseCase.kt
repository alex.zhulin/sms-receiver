package ru.alexeyzhulin.smsreceiver.domain.usecases

import ru.alexeyzhulin.smsreceiver.domain.data.SettingsData
import ru.alexeyzhulin.smsreceiver.domain.repo.SettingsDataRepository
import javax.inject.Inject

class GetSettingsDataUseCase @Inject constructor(private val settingsDataRepository: SettingsDataRepository) {
    suspend operator fun invoke(): SettingsData {
        return settingsDataRepository.getSettingsData()
    }
}