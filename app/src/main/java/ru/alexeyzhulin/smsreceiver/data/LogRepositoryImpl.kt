package ru.alexeyzhulin.smsreceiver.data

import ru.alexeyzhulin.smsreceiver.data.datasources.ApiDatasource
import ru.alexeyzhulin.smsreceiver.domain.data.LogData
import ru.alexeyzhulin.smsreceiver.domain.data.OperationResult
import ru.alexeyzhulin.smsreceiver.domain.repo.LogRepository
import javax.inject.Inject

class LogRepositoryImpl @Inject constructor(private val apiDatasource: ApiDatasource) : LogRepository {
    override suspend fun sendLogData(logData: LogData): OperationResult<Void> {
        return apiDatasource.sendLogData(logData)
    }
}