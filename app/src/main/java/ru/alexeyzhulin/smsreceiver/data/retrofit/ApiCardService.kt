package ru.alexeyzhulin.smsreceiver.data.retrofit

import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Url
import ru.alexeyzhulin.smsreceiver.domain.data.CardData
import ru.alexeyzhulin.smsreceiver.domain.data.LogData
import ru.alexeyzhulin.smsreceiver.domain.data.OperationResult

interface ApiCardService {
    @POST()
    suspend fun sendCardData(@Url url: String, @Body cardData: CardData): Response<Void>
}