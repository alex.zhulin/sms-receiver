package ru.alexeyzhulin.smsreceiver.data.datasources

import ru.alexeyzhulin.smsreceiver.domain.data.CardData
import ru.alexeyzhulin.smsreceiver.domain.data.LogData
import ru.alexeyzhulin.smsreceiver.domain.data.OperationResult

interface ApiDatasource {
    suspend fun sendLogData(logData: LogData): OperationResult<Void>

    suspend fun sendCardData(url: String, cardData: CardData): OperationResult<Void>
}