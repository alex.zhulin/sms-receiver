package ru.alexeyzhulin.smsreceiver.data

import ru.alexeyzhulin.smsreceiver.data.datasources.ApiDatasource
import ru.alexeyzhulin.smsreceiver.domain.data.CardData
import ru.alexeyzhulin.smsreceiver.domain.data.LogData
import ru.alexeyzhulin.smsreceiver.domain.data.OperationResult
import ru.alexeyzhulin.smsreceiver.domain.repo.CardRepository
import ru.alexeyzhulin.smsreceiver.domain.repo.LogRepository
import javax.inject.Inject

class CardRepositoryImpl @Inject constructor(private val apiDatasource: ApiDatasource) : CardRepository {
    override suspend fun sendCardData(url: String, cardData: CardData): OperationResult<Void> {
        return apiDatasource.sendCardData(url = url, cardData = cardData)
    }
}