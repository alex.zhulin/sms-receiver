package ru.alexeyzhulin.smsreceiver.data.datasources

import androidx.datastore.core.DataStore
import kotlinx.coroutines.flow.first
import ru.alexeyzhulin.smsreceiver.domain.data.SettingsData
import javax.inject.Inject

class LocalStorageDatasourceImpl @Inject constructor(private val dataStore: DataStore<SettingsData>) :
    LocalStorageDatasource {
    override suspend fun getSettingsData(): SettingsData {
        return try {
            dataStore.data.first()
        } catch (e: Exception) {
            SettingsData()
        }
    }

    override suspend fun setSettingsData(settingsData: SettingsData) {
        dataStore.updateData { settingsData }
    }
}