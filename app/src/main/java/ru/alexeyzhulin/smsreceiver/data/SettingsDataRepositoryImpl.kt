package ru.alexeyzhulin.smsreceiver.data

import ru.alexeyzhulin.smsreceiver.data.datasources.LocalStorageDatasource
import ru.alexeyzhulin.smsreceiver.domain.data.SettingsData
import ru.alexeyzhulin.smsreceiver.domain.repo.SettingsDataRepository
import javax.inject.Inject

class SettingsDataRepositoryImpl @Inject constructor(private val localStorageDatasource: LocalStorageDatasource) :
    SettingsDataRepository {
    override suspend fun getSettingsData(): SettingsData {
        return localStorageDatasource.getSettingsData()
    }

    override suspend fun setSettingsData(settingsData: SettingsData) {
        localStorageDatasource.setSettingsData(settingsData)
    }
}