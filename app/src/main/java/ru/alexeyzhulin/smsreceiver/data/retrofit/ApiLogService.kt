package ru.alexeyzhulin.smsreceiver.data.retrofit

import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import ru.alexeyzhulin.smsreceiver.domain.data.LogData
import ru.alexeyzhulin.smsreceiver.domain.data.OperationResult

interface ApiLogService {
    @POST("test/insertLogRecord")
    suspend fun sendLogData(@Body logData: LogData): Response<OperationResult<Void>>
}