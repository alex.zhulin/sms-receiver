package ru.alexeyzhulin.smsreceiver.data.datasources

import android.util.Log
import com.google.gson.Gson
import retrofit2.Response
import ru.alexeyzhulin.smsreceiver.data.retrofit.ApiCardService
import ru.alexeyzhulin.smsreceiver.domain.data.LogData
import ru.alexeyzhulin.smsreceiver.domain.data.OperationResult
import ru.alexeyzhulin.smsreceiver.data.retrofit.ApiLogService
import ru.alexeyzhulin.smsreceiver.domain.data.CardData
import ru.alexeyzhulin.smsreceiver.domain.data.enums.Result
import javax.inject.Inject

class ApiDatasourceImpl @Inject constructor(
    private val apiLogService: ApiLogService,
    private val apiCardService: ApiCardService
) :
    ApiDatasource {
    override suspend fun sendLogData(logData: LogData): OperationResult<Void> {
        return try {
            val response: Response<OperationResult<Void>> = apiLogService.sendLogData(logData)
            Log.d("ApiDatasourceImpl.sendLogData", "logData = ${Gson().toJson(logData)}")
            if (response.isSuccessful) {
                OperationResult(operationResult = Result.OK)
            } else {
                // TODO Решить вопрос с блокирующим методом
                val errorBody: String? = response.errorBody()?.string()
                Log.d("ApiDatasourceImpl.sendLogData", "errorBody = ${errorBody ?: "Empty string"}")
                OperationResult(operationResult = Result.ERROR)
            }
        } catch (e: Exception) {
            Log.d("ApiDatasourceImpl.sendLogData", e.localizedMessage ?: "Empty error message")
            OperationResult(operationResult = Result.ERROR)
        }
    }

    override suspend fun sendCardData(url: String, cardData: CardData): OperationResult<Void> {
        return try {
            val response: Response<Void> = apiCardService.sendCardData(url = url, cardData = cardData)
            Log.d("ApiDatasourceImpl.sendCardData", "cardData = ${Gson().toJson(cardData)}")
            if (response.isSuccessful) {
                OperationResult(operationResult = Result.OK)
            } else {
                // TODO Решить вопрос с блокирующим методом
                val errorBody: String? = response.errorBody()?.string()
                Log.d("ApiDatasourceImpl.sendLogData", "errorBody = ${errorBody ?: "Empty string"}")
                OperationResult(operationResult = Result.ERROR)
            }
        } catch (e: Exception) {
            Log.d("ApiDatasourceImpl.sendCardData", e.localizedMessage ?: "Empty error message")
            OperationResult(operationResult = Result.ERROR)
        }
    }
}