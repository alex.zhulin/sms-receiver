package ru.alexeyzhulin.smsreceiver.data.datasources

import ru.alexeyzhulin.smsreceiver.domain.data.SettingsData

interface LocalStorageDatasource {
    suspend fun getSettingsData(): SettingsData

    suspend fun setSettingsData(settingsData: SettingsData)
}