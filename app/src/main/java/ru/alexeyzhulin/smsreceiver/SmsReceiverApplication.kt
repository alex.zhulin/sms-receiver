package ru.alexeyzhulin.smsreceiver

import android.app.Application
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SmsReceiverApplication: Application() {
    companion object {
        const val APPLICATION_NAME = "SmsReceiver"
        const val BASE_URL = "https://fotozagruzka.ru/"
        const val BASE_LOG_URL = "http://api.jojojob.io:8080/api/"
        const val SHARED_PREFERENCES_FILE_NAME = "datastore/preferences.json"
        val gson: Gson = GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .create()
    }
}